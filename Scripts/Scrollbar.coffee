ko.bindingHandlers.virtualScroll =
    init: (element, valueAccessor, allBindings, viewModel, bindingContext) =>
        # This will be called once when the binding is first applied to an element,
        # and again whenever the associated observable changes value.
        # Update the DOM element based on the supplied values here.
        value = valueAccessor();
 
        # Next, whether or not the supplied model property is observable, get its current value
        valueUnwrapped = ko.unwrap(value);

        # Make a modified binding context, with a extra properties, and apply it to descendant elements
        childBindingContext = bindingContext.createChildContext(
            bindingContext.$rawData, 
            null, # Optionally, pass a string here as an alias for the data item in descendant contexts
            (context) =>
                itemsPerLine = valueUnwrapped.itemsPerLine
                lineHeight = valueUnwrapped.lineHeight
                array = ko.unwrap(valueUnwrapped.foreach)
                scrollResult = scroll(element, array, lineHeight, itemsPerLine)
                ko.utils.extend(context, {
                    arrayToShow: ko.observableArray(scrollResult.itemsToShow),
                    topPadding: ko.observable(scrollResult.topPadding)
                    bottomPadding: ko.observable(scrollResult.bottomPadding)
                    });
            );
        ko.applyBindingsToDescendants(childBindingContext, element);

        bindingContext.childBindingContext = childBindingContext

        $(element).scroll(_.throttle(()=>
            value = valueAccessor();
 
            #  Next, whether or not the supplied model property is observable, get its current value
            valueUnwrapped = ko.unwrap(value);
            
            itemsPerLine = valueUnwrapped.itemsPerLine
            lineHeight = valueUnwrapped.lineHeight
            array = ko.unwrap(valueUnwrapped.foreach)
            scrollResult = scroll(element, array, lineHeight, itemsPerLine)

            childBindingContext = bindingContext.childBindingContext
            childBindingContext.arrayToShow(scrollResult.itemsToShow)
            childBindingContext.topPadding(scrollResult.topPadding)        
            childBindingContext.bottomPadding(scrollResult.bottomPadding)
        , 100));
        
        # Also tell KO *not* to bind the descendants itself, otherwise they will be bound twice
        return { controlsDescendantBindings: true };

    update: (element, valueAccessor, allBindings, viewModel, bindingContext) =>
        # This will be called once when the binding is first applied to an element,
        # and again whenever the associated observable changes value.
        # Update the DOM element based on the supplied values here.
        value = valueAccessor();
 
        # Next, whether or not the supplied model property is observable, get its current value
        valueUnwrapped = ko.unwrap(value);
        
        itemsPerLine = valueUnwrapped.itemsPerLine
        lineHeight = valueUnwrapped.lineHeight
        array = ko.unwrap(valueUnwrapped.foreach)
        scrollResult = scroll(element, array, lineHeight, itemsPerLine)

        childBindingContext = bindingContext.childBindingContext
        childBindingContext.arrayToShow(scrollResult.itemsToShow)
        childBindingContext.topPadding(scrollResult.topPadding)        
        childBindingContext.bottomPadding(scrollResult.bottomPadding)

scroll = (element, array, lineHeight, itemsPerLine) =>
    $element = $(element)

    height = $element.height()
    scrollTop = $element.scrollTop()

    totalHeight = Math.ceil(array.length / itemsPerLine) * lineHeight

    indexOfFirstItemToShow = Math.floor(scrollTop / lineHeight)*itemsPerLine
    topOfFirstItem = indexOfFirstItemToShow * lineHeight / itemsPerLine

    overflowAtTop = (scrollTop - (Math.floor((scrollTop/lineHeight)) * lineHeight))

    
    numberOfRows = Math.ceil(height / (lineHeight)) + (overflowAtTop > 0 ? itemsPerLine : 0)
    numberOfItemsToShow = numberOfRows * itemsPerLine

    sizeOfAreaNeed = numberOfRows * lineHeight

    itemToShow = array.slice(indexOfFirstItemToShow, indexOfFirstItemToShow + numberOfItemsToShow)

    topPadding = topOfFirstItem
    bottomPadding = Math.max(0, totalHeight - topOfFirstItem - sizeOfAreaNeed)

    {
        topPadding: topPadding,
        bottomPadding: bottomPadding,
        itemsToShow: itemToShow
    }
